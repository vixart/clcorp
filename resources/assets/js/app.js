
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

// Font-Awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(faMapMarkerAlt)
Vue.component('font-awesome-icon', FontAwesomeIcon)


// Buefy
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
Vue.use(Buefy, {defaultIconPack:'fa'})
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('series-tape', require('./components/SeriesTape.vue'));
Vue.component('series', require('./components/Series.vue'));

// Vue-resource
import VueResource from 'vue-resource'
Vue.use(VueResource);

// Vue-router
import VueRouter from 'vue-router'
Vue.use(VueRouter)

// Components
import SeriesTape from './components/SeriesTape.vue'

// Router
const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/episodes/:pageNumber/:search?',
      component: SeriesTape,
      name: 'episodes'
    }
  ],
});


const app = new Vue({
    el: '#app',
    components: { SeriesTape },
    router
});
