<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
      <div id="app">
        <series-tape></series-tape>
      </div>
      <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
