<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Episode extends Model {
  protected $table = 'episodes';
  protected $fillable = ['id_series', 'title_ru', 'title_en', 'image', 'link', 'release_date'];

  public function series(){
    return $this->belongsTo('App\Series', 'id_series', 'id');
  }
}
