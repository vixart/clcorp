<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Series;
use App\Season;
use App\Episode;

use Symfony\Component\DomCrawler\Crawler;

class ParseLostfilmNew implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $link;
    private $paginationFilter;
    private $elementsFilter;
    private $xelementsFilter;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct() {
        $this->link = "https://www.lostfilm.tv/new/";
        $this->paginationFilter = "//div[@class='pagging-pane']";
        $this->elementsFilter = "//body[@class='lstfml']/div[@class='wrapper']/div[@class='content']/div[@id='left-pane']/div[@class='content history']/div[@class='center-block margin-left']/div[@class='text-block serials-list']/div[2]";
    }

    private function getPaginationMinMax() {
      $html = file_get_contents($this->link);
      $crawler = new Crawler($html);
      $pagination = $crawler->filterXPath($this->paginationFilter);
      $paginationElements = $pagination->children();

      $pagesNumber = [];
      foreach ($paginationElements as $domElement) {
        if(ctype_digit($domElement->nodeValue)) {
          $pagesNumber[] = (int)$domElement->nodeValue;
        }
      }

      return [min($pagesNumber),max($pagesNumber)];
    }

    private function savePage($html) {
      $crawler = new Crawler($html);
      $pageElements = $crawler->filterXPath($this->elementsFilter)->children('div.row > a:first-child');

      $pageElements->each(function (Crawler $node, $i) {
        $seriesData = [
          'title_ru' => $node->children('div.body > div.name-ru')->text(),
          'title_en' => $node->children('div.body > div.name-en')->text()
        ];
        $seriesWhere = [
          'title_ru' => $node->children('div.body > div.name-ru')->text(),
          'title_en' => $node->children('div.body > div.name-en')->text()
        ];
        $series = Series::updateOrCreate($seriesData,$seriesWhere);

        // $leftPart = $node->children('div.picture-box > div.overlay > div.left-part')->text();
        // $seasonData = [
        //   'id_series' => $series->id,
        //   'number' => explode(' ', $leftPart)[0]
        // ];
        // $seasonWhere = [
        //   'id_series' => $series->id,
        //   'number' => explode(' ', $leftPart)[0]
        // ];
        // $season = Season::updateOrCreate($seasonData,$seasonWhere);

        $sqlDate = date('Y-m-d', strtotime($node->children('div.picture-box > div.overlay > div.right-part')->text()));
        $episodeData = [
          'id_series' => $series->id,
          'title_ru' => $node->children('div.body > div.details-pane > div.alpha')->text(),
          'title_en' => $node->children('div.body > div.details-pane > div.beta')->text(),
          'image' => $node->children('div.picture-box > img.thumb')->attr('src'),
          'link' => $node->attr('href'),
          'release_date' => $sqlDate
        ];
        $episodeWhere = [
          'id_series' => $series->id,
          'title_ru' => $node->children('div.body > div.details-pane > div.alpha')->text(),
          'title_en' => $node->children('div.body > div.details-pane > div.beta')->text()
        ];
        $episode = Episode::updateOrCreate($episodeData,$episodeWhere);

        echo "Done: {$seriesData['title_ru']} - {$episodeData['title_ru']}\n";
      });
    }

    public function handle() {
      list($minPage, $maxPage) = $this->getPaginationMinMax();

      for($i = $minPage; $i <= $maxPage; $i++) {
        $html = file_get_contents("{$this->link}page_{$i}");
        $this->savePage($html);
        echo "End page: №$i\n";
      }

      echo "All pages is done!!!";
    }
}
