<?php

namespace App\Http\Controllers;
use App\Episode;
use App\Series;

use Illuminate\Http\Request;

class SeriesController extends Controller {
    public function episodes() {
      return view('episodes');
    }

    public function getPage($page = 1, $count = 20, $search = "") {
      $episodesCount = Episode::where('title_ru', 'like', "{$search}%")
                                ->orWhere('title_en', 'like', "{$search}%")
                                ->orWhereHas('series', function ($query) use ($search) {
                                    $query->where('title_ru', 'like', "{$search}%")
                                          ->orWhere('title_en', 'like', "{$search}%");
                                })
                                ->count();
      $episodes = Episode::where('title_ru', 'like', "{$search}%")
                           ->orWhere('title_en', 'like', "{$search}%")
                           ->orWhereHas('series', function ($query) use ($search)  {
                                 $query->where('title_ru', 'like', "{$search}%")
                                     ->orWhere('title_en', 'like', "{$search}%");
                           })
                           ->offset(($page-1) * $count)
                           ->limit($count)
                           ->orderBy('release_date', 'desc')
                           ->with('series')
                           ->get();
//      foreach ($episodes as &$episode) {
//        $episode->series = $episode->series;
//      }
      return json_encode(["total" => $episodesCount, "episodes" => $episodes]);
    }
}
