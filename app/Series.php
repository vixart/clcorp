<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Series extends Model {
    protected $table = 'series';
    protected $fillable = ['title_ru', 'title_en'];

    public function episodes() {
        return $this->hasMany('App\Episode', 'id', 'id_series');
    }
}
